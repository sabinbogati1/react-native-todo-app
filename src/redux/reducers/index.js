import {combineReducers} from "redux";
import RootReducer from '../../root/reducer';
import TodoReducer from "screens/todoDashboard/reducer";

export default combineReducers({
    rootReducer: RootReducer,
    todoReducer: TodoReducer
});