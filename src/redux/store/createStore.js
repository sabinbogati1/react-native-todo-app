import createSagaMiddleware from 'redux-saga'
import { applyMiddleware, compose, createStore } from 'redux'
import { persistReducer, persistStore } from 'redux-persist'
// import hardSet from 'redux-persist/lib/stateReconciler/hardSet'

import storage from 'redux-persist/es/storage'
import { createLogger } from 'redux-logger'

export default (rootReducer, rootSaga) => {
  const middleware = []
  const enhancers = []

  // Connect the sagas to the redux store
  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  const logger = createLogger({
    timestamps: true,
    collapsed: true,
    duration: true,
    diff: true,
  })

  const persistConfig = {
    key: 'root',
    storage,
  }

  if (__DEV__) {
    middleware.push(logger)
  }

  enhancers.push(applyMiddleware(...middleware))

  const persistedReducer = persistReducer(persistConfig, rootReducer)

  // Redux persist
  // const store = createStore(persistedReducer, undefined, compose(...enhancers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

  const store = createStore(persistedReducer, undefined, compose(...enhancers))

  const persistor = persistStore(store)

  // Kick off the root saga
  sagaMiddleware.run(rootSaga)

  return { store, persistor }
}
