import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import CartScreen from 'screens/Cart';
import AccountScreen from 'screens/Account';
import MessageScreen from 'screens/Message';
import {DashboardStack} from 'navigation/navigator-stacks/dashboardStack';

const BottomTabStack = createBottomTabNavigator(
  {
    Home: DashboardStack,
    Cart: CartScreen,
    Message: MessageScreen,
    Accounts: AccountScreen,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home`;
        } else if (routeName === 'Accounts') {
          iconName = `ios-person`;
        } else if (routeName === 'Cart') {
          iconName = `ios-cart`;
        } else if (routeName === 'Message') {
          iconName = `ios-mail`;
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#1a75ff',
      inactiveTintColor: 'gray',
    },
  },
);

export {BottomTabStack};
