import React, {Component} from 'react';
import AppNav from './main-navigators';
import {StatusBar, View} from 'react-native';
import NavigationService from './navigationService';

class AppNavigator extends Component {
  render() {
    return (
      <View style={styles.container}>
        <AppNav
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </View>
    );
  }
}

export default AppNavigator;

const styles = {
  container: {
    flex: 1,
  },
};
