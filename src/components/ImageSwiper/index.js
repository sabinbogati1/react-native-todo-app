import React, {Component} from 'react';
import {SliderBox} from 'react-native-image-slider-box';

class ImageSwiper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        'https://source.unsplash.com/1024x768/?nature',
        'https://source.unsplash.com/1024x768/?water',
        'https://source.unsplash.com/1024x768/?girl',
        'https://source.unsplash.com/1024x768/?tree',
      ],
    };
  }
  render() {
    return (
      <SliderBox
        circleLoop
        dotColor="#cc3300"
        // style={this.props.style}
        sliderBoxHeight={this.props.height}
        inactiveDotColor="#FFF"
        paginationBoxVerticalPadding={20}
        paginationBoxStyle={{
          position: 'absolute',
          bottom: 0,
          padding: 0,
          alignSelf: 'flex-start',
          paddingVertical: 10,
        }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          padding: 0,
          margin: 0,
          backgroundColor: '#FFFrgba(128, 128, 128, 0.92)',
        }}
        images={this.state.images}
      />
    );
  }
}

export {ImageSwiper};
