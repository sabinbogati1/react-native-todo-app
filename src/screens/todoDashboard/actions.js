import { ADD_TASK, DELETE_TASK, COMPLETED_TASK, CLEAR_COMPLETED_TASK } from "./actionType";


export const addTask = newTaskList => {
    return {
        type:ADD_TASK,
        payload:newTaskList
    }
}

export const deleteTask = newTaskList => {
    return {
        type:DELETE_TASK,
        payload: newTaskList
    }
}


export const completedTask = (data) => {
    return {
        type: COMPLETED_TASK,
        payload: data
    }
}

export const clearCompletedTask = newComTaskList =>{
    return {
        type:CLEAR_COMPLETED_TASK,
        payload:newComTaskList
    }
}