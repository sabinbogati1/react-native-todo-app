import React, {Component} from 'react';

import FavoriteProductComponent from './components';

class FavoriteProductContainer extends Component {
  state = {
    searchText: '',
  };

  _setSearchText = searchText => {
    this.setState({
      searchText: searchText,
    });
  };

  _searchFilter = () => {
    console.log('search pressed');
  };
  render() {
    return (
      <FavoriteProductComponent
        {...this.state}
        {...this.props}
        searchFilter={this._searchFilter}
        setSearchText={this._setSearchText}
      />
    );
  }
}

export default FavoriteProductContainer;
