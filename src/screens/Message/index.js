import React, {Component} from 'react';
import {View, SafeAreaView, StatusBar, Text} from 'react-native';

import MessageComponent from './components';

class MessageContainer extends Component {
  _heartButtonCall = () => {
    console.log('heart Call pressed');
  };

  _locationPinCall = () => {
    console.log('locationPin Call pressed');
  };

  _thumbButtonCall = () => {
    console.log('thumbButton Call pressed');
  };

  render() {
    return (
      <MessageComponent
        thumbButtonCall={this._thumbButtonCall}
        heartButtonCall={this._heartButtonCall}
        locationPinCall={this._locationPinCall}
      />
    );
  }
}

export default MessageContainer;
